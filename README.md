# Paragrafos explicativos das ferramentas e afins: #

### Extreme programming - XP ###

O xp � uma metodologia para desenvolvimento de software, para melhorar a qualidade do software, que fundamenta suas pr�ticas nos valores: Comunica��o, simplicidade, feedback, coragem e respeito.  A ideia da XP � mostrar para os envolvidos que � preciso adotar ao m�ximo um conjunto de pr�ticas que s�o consideradas boas no contexto da engenharia de software. S�o essas: Planning game(priorizar as funcionalidades), pequenas releases, met�foras, design simples, testes de aceita��o, constru�dos juntamente com os clientes, n�o mais que 40h p/ semana, horas extras s�o permitidas apenas se for impactar positivamente o projeto, propriedade coletiva(o c�digo n�o tem dono), programa��o pareada(duas pessoas em um mesmo computador, um inspeciona o c�digo do outro) e padroniza��o de c�digo que � definida antes de iniciar a codifica��o e todos devem segui-la.  

### Kanban ###

Kanban � uma forma de fazer coisas, eficaz e pode dar suporte num processo de produ��o como um todo. � uma representa��o visual de informa��es em um quadro, que possibilita que os membros da equipe vejam como o trabalho est� fluindo dentro do processo a qualquer momento, levando � uma transpar�ncia total do trabaho, uma maior comunica��o e colabora��o entre os integrantes do time. Tamb�m funciona bem quando usado juntamente com o scrum.

### Gitflow e Gitlabflow processes ###

S�o frameworks que foram desenvolvido para trabalhar em conjunto com o controle de vers�o Git, s�o modelos que podem realmente ajudar a gerenciar um projeto e otimizar o fluxo de trabalho na equipe. 

Para que padr�es sejam garantidos existem diversos modelos de branches models que controlam e definem estrat�gias para as diversas possibilidades de ramifica��o de fluxos de trabalho.

Existe um grande desafio em garantir de que o modelo ser� seguido pelo time. O problema de se trabalhar com esses fluxos � a possibilidade de existir conflitos constantemente.

O Gitflow trabalha com diferentes branches para gerenciar facilmente cada fase do desenvolvimento de software, � sugerido para ser usado quando seu software tem o conceito de "release" porque n�o � a melhor decis�o quando voc� trabalha no ambiente de Entrega Cont�nua ou de Implementa��o Cont�nua , onde este conceito est� ausente, al�m de ser ideal para projetos em equipe e cada um precise colaborar com o mesmo recurso, pois permite que cada um trabalhe na sua feature e consiga resolver bugs quando encontrados.  
 
A principal diferen�a entre os dois frameworks est� na forma em que ser�o aplicados dependendo da complexidade do projeto. A forma em que as branches s�o criadas e mescladas tamb�m diferem e os tipos, pois no gitflow existem cinco tipos: Master, Develop, Feature, Hotfix e Release. J� no Gitlab flow o processo � mais simples.


### JSON YAML ###

* O JSON tem uma documenta��o mais extensa e � mais usado e suportado que o YAML;

* O JSON � mais indicado para comunica��o entre um cliente e um servidor, j� o YAML para arquivos de configura��o, pois � mais leg�vel;

* O YAML precisa ser identado com espa�os, usar o TAB pode ocasionar erros;

* O YAML pode precisar de bibliotecas para funcionar al�m de ser menos perform�tico pois existem v�rias maneiras de representar e fazer hierarquia de dados, por isso o processamento � mais complexo quando comparado ao JSON;

* O YAML possui recursos adicionais que faltam no JSON, como por exemplo: Coment�rios, tipos de dados extens�veis, �ncoras relacionais, strings sem aspas e tipos de mapeamento que preservam a ordem das chaves; 

* O YAML � melhor para cria��o r�pida e compreens�o de arquivos de configura��o simples de m�dulos de software;

* O JSON � melhor para implementa��o r�pida e implementa��o de transfer�ncia de dados simples entre m�dulos de software.


# Links de refer�ncia #

* [Markdown Editor Online](https://jbt.github.io/markdown-editor/)
* [Extreme Programming(XP)-V�deo](https://www.youtube.com/watch?v=ALvpFMcL-dI)
* [Extreme Programming(XP)](https://hiperbytes.com.br/xp/)
* [Kanban-V�deo](https://www.youtube.com/watch?v=R8dYLbJiTUE)
* [O que � Kanban?](https://www.atlassian.com/agile/kanban)
* [What is kanban?](https://leankit.com/learn/kanban/what-is-kanban/)
* [Gitflow - fluxo de desenvolvimento](https://imasters.com.br/gerencia-de-ti/fluxo-de-desenvolvimento-com-gitflow/?trace=1519021197) 
* [Gitflow](https://fjorgemota.com/git-flow-uma-forma-legal-de-organizar-repositorios-git/)
* [Gitlabflow](https://about.gitlab.com/2014/09/29/gitlab-flow/)
* [Regras do Gitlabflow](https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/)
* [Git Flow vs Github Flow](https://lucamezzalira.com/2014/03/10/git-flow-vs-github-flow/)
* [YAML Ain�t Markup Language (YAML�) Version 1.2](http://yaml.org/spec/1.2/spec.html)
* [Introdu��o ao JSON](https://www.json.org/json-pt.html)
* [YAML vs JSON](http://sangsoonam.github.io/2017/03/13/yaml-vs-json.html)

